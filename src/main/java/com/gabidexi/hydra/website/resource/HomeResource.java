/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gabidexi.hydra.website.resource;

import com.gabidexi.hydra.website.services.spellcheck.Check;
import com.gabidexi.hydra.website.services.spellcheck.DocumentSummary;
import com.gabidexi.hydra.website.view.HomeView;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.xml.ws.WebServiceRef;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author mvie
 */
@Path("/")
public class HomeResource {
    @WebServiceRef(wsdlLocation = "http://wsf.cdyne.com/SpellChecker/check.asmx?WSDL")
    private final Check service = new Check();
   
    /*@GET
    public HomeView getHome() {
        return new HomeView();
    }*/
    
    @GET
    public HomeView getSpelling(@QueryParam("word") @NotEmpty String word) {
        DocumentSummary doc = service.getCheckSoap().checkTextBodyV2(word);
        if(doc.getMisspelledWordCount() > 0) {
            List<String> suggestions = doc.getMisspelledWord().get(0).getSuggestions();
            return new HomeView(suggestions);
        }
        return new HomeView(Arrays.asList("Horse", "Purse"));
    }
}
