/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gabidexi.hydra.website.resource;

import com.gabidexi.hydra.website.view.BaseView;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 *
 * @author mvie
 */
@Path("/features")
public class FeatureResource {
    
    @GET
    public BaseView getFeatures() {
        return new BaseView("features.mustache");
    }
}
