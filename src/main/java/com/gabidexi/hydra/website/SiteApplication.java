/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gabidexi.hydra.website;

import com.gabidexi.hydra.website.resource.*;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;

/**
 *
 * @author mvie
 */
public class SiteApplication extends Application<SiteConfiguration> {
    public static void main(String[] args) throws Exception {
        new SiteApplication().run(args);
    }
    
    @Override
    public String getName() {
        return "project-hydra";
    }
    
    @Override
    public void initialize(Bootstrap<SiteConfiguration> bootstrap) {
        bootstrap.addBundle(new ViewBundle<SiteConfiguration>());
        bootstrap.addBundle(new AssetsBundle("/assets", "/static"));
    }

    @Override
    public void run(SiteConfiguration conf, Environment e) throws Exception {
        e.jersey().register(HomeResource.class);
        e.jersey().register(FeatureResource.class);
    }
    
}
