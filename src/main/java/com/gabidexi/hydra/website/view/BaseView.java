/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gabidexi.hydra.website.view;

import ch.qos.logback.classic.util.ContextInitializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gabidexi.hydra.website.resource.FeatureResource;
import io.dropwizard.views.View;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.core.UriBuilder;

/**
 *
 * @author mvie
 */
public class BaseView extends View {
    public BaseView(String template) {
        super(template);
    }
    
    public List<MenuItem> getMenu() {
        System.out.println("Loading menu");
        return Arrays.asList(
                new MenuItem("Features", UriBuilder.fromResource(FeatureResource.class).build().toString()),
                new MenuItem("Pricing", "ko.html"),
                new MenuItem("Login", "what.html")
        );
    }
    
    public class MenuItem {
        private final String label;
        private final String url;
        
        public MenuItem(String label, String url) {
            this.label = label;
            this.url = url;
        }
        
        public String getLabel() {return label;}
        public String getUrl() {return url;}
    }
}
