/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gabidexi.hydra.website.view;

import io.dropwizard.views.View;
import java.util.List;

/**
 *
 * @author mvie
 */
public class HomeView extends BaseView {
    private List<String> words;
    public HomeView(List<String> words) {
        super("home.mustache");
        this.words = words;
    }
    
    public String getGreeting() {
        return "Welcome!";
    }
    
    public List<String> getCorrectWords() {
        return words;
    }
}
